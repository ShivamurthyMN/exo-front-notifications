/* tests */
/* DO NOT MODIFY */


Notifications.notify('By default there and first (at the bottom)');

var loading = Notifications.notify('Loading...', 100);

setTimeout(function() {
	Notifications.notify('Arriving after 2 seconds.', 10);
}, 2000);

setTimeout(function() {
	try {
		if (!loading) {
			throw new Error('');
		}
		loading.remove();
	} catch (e) {
		Notifications.notify('Remove() not implemented', 10, 'error')
	}
}, 5000);

setTimeout(function() {
	Notifications.notify('<b>html</b> to stay 15 seconds', 15);
	Notifications.notify('Error for 10 seconds', 10, 'error');

	Notifications.notify('Example warning message', 3, 'warn');

}, 1000);